﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ksrmsangati.Data.Migrations
{
    public partial class pin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Pin",
                table: "Teams",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Salt",
                table: "Teams",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pin",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "Salt",
                table: "Teams");
        }
    }
}
