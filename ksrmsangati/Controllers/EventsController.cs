using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ksrmsangati.Models;
using ksrmsangati.Data;
using Newtonsoft.Json;
using PaulMiami.AspNetCore.Mvc.Recaptcha;
using System.Security.Cryptography;
using MimeKit;
using MailKit.Net.Smtp;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace ksrmsangati.Controllers
{
    public class EventsController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IHostingEnvironment _environment;
        private ILogger _logger;
        public EventsController(ApplicationDbContext db, IHostingEnvironment environment, ILoggerFactory loggerFactory)
        {
            _db = db;
            _environment = environment;
            _logger = loggerFactory.CreateLogger(typeof(EventsController));
        }
        public IActionResult Index()
        {
            IList<EventDTO> events = _db.Events.Select(e =>
               new EventDTO
               {
                   EventId = e.EventId,
                   EventName = e.EventName,
                   EventTime = e.EventTime,
                    //Teams = e.Teams,
                    Venue = e.Venue
               }

               ).ToList();
            return View(events);
        }

        [HttpGet]
        public IActionResult Event(int eventId)
        {
            EventDTO e = _db.Events.Select(
                a => new EventDTO
                {
                    EventId = a.EventId,
                    EventName = a.EventName,
                    EventTime = a.EventTime,
                    Venue = a.Venue
                }
                ).First(x => x.EventId == eventId);
            return View(e);
        }

        [HttpGet]
        public IActionResult Register(int eventId)
        {
            try
            {
                EventDTO e = _db.Events.Select(
                a => new EventDTO
                {
                    EventId = a.EventId,
                    EventName = a.EventName,
                    EventTime = a.EventTime,
                    Venue = a.Venue
                }
                ).First(x => x.EventId == eventId);
                RegisterBindingModel model = new RegisterBindingModel { EventId = eventId, EventName = e.EventName };
                //ViewData["EventId"] = eventId;
                //ViewData["ReturnUrl"] = Url.Action("Register", "Events", new { eventId = model.EventId });
                return View(model);
            }
            catch(Exception e)
            {
                return View(null);
            }
        }

        [ValidateRecaptcha]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register([FromForm]RegisterBindingModel model)
        {
            if (ModelState.IsValid)
            {
                bool IsFileUploaded = false;
                if(model.EventId == 101 )
                {
                    if(model.FileUp==null)
                    {
                        ModelState.AddModelError("FileUp", "Please select presentation to be uploaded");
                        return View(model);
                    }
                    IsFileUploaded = true;
                }
                Team team = new Team
                {
                    TeamName = model.TeamName,
                    RegistrationTime = DateTime.UtcNow.AddHours(5).AddMinutes(30),
                    CollegeName = model.CollegeName,
                    EventId = model.EventId,
                    Email = model.Email,
                    Member1 = model.Member1,
                    Member2 = model.Member2,
                    Member3 = model.Member3,
                    Member4 = model.Member4,
                    Member5 = model.Member5,
                    PhoneNumber = model.PhoneNumber,
                    IsConfirmed = false
                };

                team.MemberCount = 2;
                if (model.Member3 != "" && model.Member3!=null)
                {
                    team.MemberCount++;
                    if (model.Member4 != "" && model.Member4!=null)
                        team.MemberCount++;
                }

                Random rdm = new Random();
                var no = rdm.Next(100000, 999999);
                team.Pin = no.ToString();
                
                _db.Teams.Add(team);
                _db.SaveChanges();

                _db.Entry(team).Reload();
                string eventName = _db.Events.Single(ev => ev.EventId == model.EventId).EventName;
                TeamDTO t = new TeamDTO
                {
                    TeamId = team.TeamId,
                    CollegeName = team.CollegeName,
                    Email = team.Email,
                    EventId = team.EventId,
                    IsConfirmed = team.IsConfirmed,
                    Member1 = team.Member1,
                    Member2 = team.Member2,
                    Member3 = team.Member3,
                    Member4 = team.Member4,
                    Member5 = team.Member5,
                    MemberCount = team.MemberCount,
                    PhoneNumber = team.PhoneNumber,
                    RegistrationTime = team.RegistrationTime,
                    TeamName = team.TeamName,
                    EventName = eventName
                };

                if (IsFileUploaded)
                {
                    var uploads = Path.Combine(_environment.WebRootPath, "uploads");
                    var file = model.FileUp;
                    if (file.Length > 0)
                    {
                        using (var fileStream = new FileStream(Path.Combine(uploads, t.TeamId + "_" + file.FileName), FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                    }
                }

                SendMail(team.Pin,t);

                TempData["Team"] = JsonConvert.SerializeObject(t);
                return RedirectToAction("RegisterSuccess");
            }
            //ViewData["ReturnUrl"] = Url.Action("Register", "Events", new { eventId = model.EventId });
            EventDTO e = _db.Events.Select(
                a => new EventDTO
                {
                    EventId = a.EventId,
                    EventName = a.EventName,
                    EventTime = a.EventTime,
                    Venue = a.Venue
                }
                ).First(x => x.EventId == model.EventId);
            model.EventName = e.EventName;
            return View(model);
        }

        [HttpGet]
        public IActionResult RegisterSuccess()
        {
            TeamDTO team = JsonConvert.DeserializeObject<TeamDTO>(TempData["Team"].ToString());
            string eventName = _db.Events.Single(ev => ev.EventId == team.EventId).EventName;
            team.EventName = eventName;
            return View(team);
        }

        public async void SendMail(string pin,TeamDTO t)
        {
            try
            {
                //From Address  
                string FromAddress = "sangati2k18@gmail.com";
                string FromAdressTitle = "Sangati 2k18";
                //To Address  
                string ToAddress = t.Email;
                string ToAdressTitle = "";
                string Subject = "Welcome to Sangati 2k18.";
                string BodyContent = @"Cheers Team!
                                        <br>
                                        You have succesfully registered to Sangati for Event - " + t.EventName.ToUpper() + ". <br>" +
                                            "Your registration ID is " + t.TeamId +
                                            ". For any queries feel free to visit <a href=\"http://ksrmsangati.com\">ksrmsangati.com</a>" +
                                            @" <br/> It's a pleasure to have you on board with us.
                                                        <br/> We at Sangati believe in providing an ambience that helps young entrepreneurs to bloom and make a strong forced bridge that not onlyempowers rural markets but also helps such rural talents dig its rott deep into the marketing sectors. <br/>
                                                        <br/> With all that said and done, 
                                                            <br/> MAY THE BEST TEAM WIN!";
                   
                //Smtp Server  
                string SmtpServer = "smtp.gmail.com";
                //Smtp Port Number  
                int SmtpPortNumber = 587;

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(FromAdressTitle, FromAddress));
                mimeMessage.To.Add(new MailboxAddress(ToAdressTitle, ToAddress));
                mimeMessage.Subject = Subject;
                mimeMessage.Body = new TextPart("html")
                {
                    Text = BodyContent

                };

                //SELF MAIL

                //From Address  
                string FromAddressSelf = "sangati2k18@gmail.com";
                string FromAdressTitleSelf = "Sangati 2k18";
                //To Address  
                string ToAddressSelf = "sangati2k18@gmail.com";
                string ToAdressTitleSelf = "";
                string SubjectSelf = "New Registration for event ";
                string BodyContentSelf = @"Cheers Team!
                                        <br>
                                        New team registered to Sangati for Event - " + t.EventName.ToUpper() + ". <br>" +
                                            "Registration ID is " + t.TeamId + "<br>"+
                                            "COLLEGE : "+t.CollegeName + " <br>" + 
                                           
                                            "" +
                                            @" ";

                //Smtp Server  
                //string SmtpServer = "smtp.gmail.com";
                //Smtp Port Number  
                //int SmtpPortNumber = 587;

                var mimeMessageSelf = new MimeMessage();
                mimeMessageSelf.From.Add(new MailboxAddress(FromAdressTitleSelf, FromAddressSelf));
                mimeMessageSelf.To.Add(new MailboxAddress(ToAdressTitleSelf, ToAddressSelf));
                mimeMessageSelf.Subject = SubjectSelf;
                mimeMessageSelf.Body = new TextPart("html")
                {
                    Text = BodyContentSelf

                };

                using (var client = new SmtpClient())
                {

                    await client.ConnectAsync(SmtpServer, SmtpPortNumber, false);
                    // Note: only needed if the SMTP server requires authentication  
                    // Error 5.5.1 Authentication   
                    await client.AuthenticateAsync("sangati2k18@gmail.com", "Makeitbigteam78");
                    await client.SendAsync(mimeMessage);
                    await client.SendAsync(mimeMessageSelf);
                    await client.DisconnectAsync(true);

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(1, ex, "Cannot send email to {0} with team id {1}.", t.Email, t.TeamId);
            }
        }
    }
}