﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ksrmsangati.Data;
using ksrmsangati.Models;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Logging;

namespace ksrmsangati.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db;
        private ILogger _logger;
        public HomeController(ApplicationDbContext db, ILoggerFactory loggerFactory)
        {
            _db = db;
            _logger = loggerFactory.CreateLogger(typeof(HomeController));
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Events()
        {
            IList<EventDTO> events = _db.Events.Select(e=>
                new EventDTO
                {
                    EventId = e.EventId,
                    EventName = e.EventName,
                    EventTime = e.EventTime,
                    //Teams = e.Teams,
                    Venue = e.Venue
                }
                
                ).ToList();
            return View(events);
        }

        public IActionResult Gallery()
        {
            return View();
        }

        public IActionResult Error()
        {
            // Get the details of the exception that occurred
            var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionFeature != null)
            {
                // Get which route the exception occurred at
                string routeWhereExceptionOccurred = exceptionFeature.Path;

                // Get the exception that occurred
                Exception exceptionThatOccurred = exceptionFeature.Error;

                _logger.LogError(0,exceptionThatOccurred,"*********************** Error Occured at  {0} : ",routeWhereExceptionOccurred);
                // TODO: Do something with the exception
                // Log it with Serilog?
                // Send an e-mail, text, fax, or carrier pidgeon?  Maybe all of the above?
                // Whatever you do, be careful to catch any exceptions, otherwise you'll end up with a blank page and throwing a 500
            }
                return View();
        }
    }
}
