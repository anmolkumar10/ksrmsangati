using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ksrmsangati.Data;
using ksrmsangati.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace ksrmsangati.Controllers
{
    
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _db;
        private ILogger _logger;
        private IHostingEnvironment _environment;

        public AdminController(ApplicationDbContext db, ILoggerFactory loggerFactory, IHostingEnvironment env)
        {
            _db = db;
            _logger = loggerFactory.CreateLogger(typeof(AdminController));
            _environment = env;
        }
        public IActionResult Index()
        {
            //_logger.LogError("Admin Section Accessed!");
            //var teams = _db.Teams.Include(t => t.Event).OrderBy(t=>t.EventId).GroupBy(t=>t.EventId).ToList();

            //var uploads = Path.Combine(_environment.WebRootPath, "uploads");
            //foreach (string file in Directory.EnumerateFiles(
            //uploads,
            //("*"),
            //SearchOption.AllDirectories)
            //)
            //{
            //    // do something

            //}
            return View();
        }
        [HttpPost]
        public IActionResult Index(string pass)
        {
            //_logger.LogError("Admin Section Accessed!");
            //var teams = _db.Teams.Include(t => t.Event).OrderBy(t=>t.EventId).GroupBy(t=>t.EventId).ToList();

            //var uploads = Path.Combine(_environment.WebRootPath, "uploads");
            //foreach (string file in Directory.EnumerateFiles(
            //uploads,
            //("*"),
            //SearchOption.AllDirectories)
            //)
            //{
            //    // do something

            //}
            if(pass=="Makeitbigteam78*")
            {
                var teams = _db.Teams.Include(t => t.Event).OrderBy(t => t.EventId).GroupBy(t => t.EventId).ToList();
                return View(teams);
            }
            return View();
        }

        public IActionResult DownloadWriteup(int id)
        {
            var uploads = Path.Combine(_environment.WebRootPath, "uploads");
            var file = Directory.GetFiles(uploads, (id + "_*"));
            if (file.Length > 0)
            {
                var fileName = Path.GetFileName(file[0]);
                var fs = System.IO.File.OpenRead(file[0]);
                return File(fs, "application/octet-stream", fileName);
            }
            return RedirectToAction("Index");
        }
    }
}