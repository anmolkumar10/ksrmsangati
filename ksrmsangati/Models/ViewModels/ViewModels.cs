﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ksrmsangati.Models.ViewModels
{
    public class EventViewModel
    {
        public int EventId { set; get; }
        public string EventName { get; set; }
        public DateTime? EventTime { get; set; }
        public string Venue { get; set; }
        public IList<Team> Teams { get; set; }
    }
}
