﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ksrmsangati.Models
{
    public class Event
    {
        public int EventId { set; get; }
        public string EventName { get; set; }
        public DateTime? EventTime { get; set; }
        public string Venue { get; set; }
        public IList<Team> Teams { get; set; }
    }

    public class EventDTO
    {
        public int EventId { set; get; }
        public string EventName { get; set; }
        public DateTime? EventTime { get; set; }
        public string Venue { get; set; }
        //public IList<TeamDTO> Teams { get; set; }
    }

    public class Team
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public DateTime? RegistrationTime { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string CollegeName { get; set; }
        public bool IsConfirmed { get; set; }
        [ForeignKey("Event")]
        public int EventId { get; set; }
        public Event Event { get; set; }
        public string Member1 { get; set; }
        public string Member2 { get; set; }
        public string Member3 { get; set; }
        public string Member4 { get; set; }
        public string Member5 { get; set; }
        public int MemberCount { get; set; }

        public string Pin { get; set; }
        public string Salt { get; set; }
    }

    public class TeamDTO
    {
        public int TeamId { get; set; }
        [Required]
        public string TeamName { get; set; }
        public DateTime? RegistrationTime { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string CollegeName { get; set; }
        public bool IsConfirmed { get; set; }
        [ForeignKey("Event")]
        public int EventId { get; set; }
        public string Member1 { get; set; }
        public string Member2 { get; set; }
        public string Member3 { get; set; }
        public string Member4 { get; set; }
        public string Member5 { get; set; }
        public int MemberCount { get; set; }
        public string EventName { get; set; }
    }

    public class RegisterBindingModel
    {
        [HiddenInput]
        [Required]
        public int EventId { get; set; }
        public string EventName { get; set; }
        public int TeamId { get; set; }
        [Required]
        [DisplayName("Team Name")]
        [Display(Prompt = "Team Name")]
        public string TeamName { get; set; }
        public DateTime? RegistrationTime { get; set; }
        [Required]
        [Phone]
        [DisplayName("Mobile Number")]
        [Display(Prompt = "Mobile Number")]
        public string PhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        [DisplayName("Email")]
        [Display(Prompt ="Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("College Name")]
        [Display(Prompt = "College Name")]
        public string CollegeName { get; set; }
        public bool IsConfirmed { get; set; }
        [Required]
        [DisplayName("1st Member Name")]
        [Display(Prompt = "1st Member Name")]
        public string Member1 { get; set; }
        [Required]
        [DisplayName("2nd Member Name")]
        [Display(Prompt = "2nd Member Name")]
        public string Member2 { get; set; }
        [DisplayName("3rd Member Name")]
        [Display(Prompt = "3rd Member Name")]
        public string Member3 { get; set; }
        [DisplayName("4th Member Name")]
        [Display(Prompt = "4th Member Name")]
        public string Member4 { get; set; }
        public string Member5 { get; set; }

        [DisplayName("Upload your writeup (<=20MB)")]        
        public IFormFile FileUp { get; set; }
    }
}
